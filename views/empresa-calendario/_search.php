<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\calendario\models\search\EmpresaCalendarioSearch */
/* @var $form ActiveForm */
?>

<div class="empresa-calendario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get', 'options' => ['data-pjax' => 1],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'empresa_id') ?>

    <?= $form->field($model, 'fecha_vencimiento') ?>

    <?= $form->field($model, 'frecuencia') ?>

    <?= $form->field($model, 'detalle') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
