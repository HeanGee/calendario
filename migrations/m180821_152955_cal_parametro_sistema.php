<?php

use jamband\schemadump\Migration;

class m180821_152955_cal_parametro_sistema extends Migration
{
    public function safeUp()
    {
        // cal_parametro_sistema
        $this->createTable('{{%cal_parametro_sistema}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull()->unique(),
            'valor' => $this->string(45)->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo "m180821_152955_cal_parametro_sistema no puede ser revertido.\n";
        return false;
    }
}
