<?php

use jamband\schemadump\Migration;

class m190527_154625_addcol_calendario extends Migration
{
    public function safeUp()
    {
        $table = 'cal_empresa_calendario';
        $this->addColumn($table, 'usuario_id', $this->integer(10)->unsigned()->null()->comment("ID de usuario asignado a la tarea."));
        $this->addForeignKey('fk_cal_usuario_id', $table, 'usuario_id', 'core_usuario', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
