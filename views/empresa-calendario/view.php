<?php

use backend\modules\calendario\models\EmpresaCalendario;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use common\models\User;
use kartik\date\DatePicker;
use kartik\detail\DetailView;
use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\calendario\models\EmpresaCalendario */

$this->title = 'Calendario de Actividades';
$this->params['breadcrumbs'][] = ['label' => 'Calendario de Actividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="empresa-calendario-view">

    <p>
        <?= !PermisosHelpers::getAcceso('calendario-empresa-calendario-index') ? "" : Html::a('Index', ['index',], ['class' => 'btn btn-info']) ?>
        <?= !PermisosHelpers::getAcceso('calendario-empresa-calendario-update') ? "" : Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= !PermisosHelpers::getAcceso('calendario-empresa-calendario-delete') ? "" : Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de querer borrar?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <?php
    try {
        echo DetailView::widget([
            'model' => $model, /* 'condensed' => true,*/
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Calendario',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [['attribute' => 'empresa_id', 'value' => $model->empresa->nombre],
                [
                    'attribute' => 'fecha_vencimiento',
                    'value' => date('d-m-Y', strtotime($model->fecha_vencimiento))
                ],
                'frecuencia',
                'detalle', ['attribute' => 'estado',],

                [
                    'attribute' => 'usuario_id',
                    'label' => "Usuario asignado",
                    'value' => ($model->usuario_id != '') ? User::findOne($model->usuario_id)->username : '',
                ],
            ],
        ]);

        $template = '';
        foreach (['view', 'update', 'delete'] as $_v) if (PermisosHelpers::getAcceso("empresa-calendario-{$_v}")) $template .= "&nbsp{{$_v}}";

        Pjax::begin(['id' => 'sub-tareas-grid']);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'export' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                ['attribute' => 'nombre_empresa',
                    'value' => 'empresa.nombre',
                    'label' => 'Empresa',
                    'contentOptions' => ['style' => 'width: 30%; text-align:left'],
                ],
                [
                    'attribute' => 'fecha_vencimiento',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_vencimiento',
                        'language' => 'es',
                        'pluginOptions' => [
                            'orientation' => 'bottom center',
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => 0,
                        ]
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return date('d-m-Y', strtotime($model->fecha_vencimiento));
                    },
                    'contentOptions' => ['style' => 'width: 13%; text-align:center'],
                ],
                [
                    'attribute' => 'no_vencido',
                    'label' => "Por Vencer?",
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var $model EmpresaCalendario */
                        $fa_icon = ($model->fecha_vencimiento < date('Y-m-d') ? 'fa fa-times text-danger' : 'fa fa-check text-success');
                        return "<span class='$fa_icon'></span>";
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'no_vencido',
                        'data' => ['' => 'Todos', 'si' => 'Si', 'no' => 'No'],
                        'hideSearch' => true
                    ]),
                    'contentOptions' => ['style' => 'width: 5%; text-align:center'],
                ],
                [
                    'attribute' => 'frecuencia',
                    'value' => function ($model, $key, $index, $column) {
                        return ucfirst($model->frecuencia);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'frecuencia',
                        'data' => ['unico' => 'Único'],
                        'hideSearch' => true
                    ]),
                    'contentOptions' => ['style' => 'width: 8%; text-align:left'],
                ],
                [
                    'attribute' => 'detalle',
                    'contentOptions' => ['style' => 'width: 30%; text-align:left'],
                ],

                [
                    'class' => 'kartik\grid\EditableColumn',
                    'header' => 'Estado (editable)',
                    'attribute' => 'estado',
                    'value' => function ($model) {
                        return $model->estado;
                    },
                    'editableOptions' => function ($model, $key, $index) {
                        return [
                            'format' => Editable::FORMAT_LINK,
                            'asPopover' => false,
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => ['activo' => "Activo", 'inactivo' => "Inactivo"],
                            'options' => ['class' => 'form-control', 'prompt' => 'Seleccione un estado...'],
                            'displayValueConfig' => [
                                'activo' => 'Activo',
                                'inactivo' => 'Inactivo',
                            ],
                            'editableButtonOptions' => [
                                'pluginEvents' => [
                                    "editableChange" => "function(event, val) { log('Changed Value ' + val); }",
                                ],
                            ],
                        ];
                    }
                ],

                ['class' => 'yii\grid\ActionColumn', 'buttons' => [], 'template' => $template,]
            ],
        ]);
        Pjax::end();
    } catch (Exception $e) {
        echo $e;
        FlashMessageHelpsers::createWarningMessage($e->getMessage());
    } ?>

</div>
