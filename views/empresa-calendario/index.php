<?php

use backend\modules\calendario\models\EmpresaCalendario;
use common\helpers\PermisosHelpers;
use common\models\User;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\calendario\models\search\EmpresaCalendarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendario de Actividades';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="empresa-calendario-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Crear Calendario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<?php
try {
    $template = '';
    foreach (['view', 'update', 'delete'] as $_v) if (PermisosHelpers::getAcceso("calendario-empresa-calendario-{$_v}")) $template .= "&nbsp{{$_v}}";

    Pjax::begin(['id' => 'calendario-grid']);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            [
//                'class' => 'kartik\grid\ExpandRowColumn',
//                'headerOptions' => ['class' => 'kartik-sheet-style'],
//                'expandOneOnly' => true,
//                'width' => '50px',
//                'value' => function ($model, $key, $index, $column) {
//                    return GridView::ROW_COLLAPSED;
//                },
//                // uncomment below and comment detail if you need to render via ajax
//                // 'detailUrl'=>Url::to(['/site/book-details']),
//                'detail' => function ($model, $key, $index, $column) {
//                    return Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
//                },
//            ],

            'id',
            ['attribute' => 'nombre_empresa',
                'value' => 'empresa.nombre',
                'label' => 'Empresa',
                'contentOptions' => ['style' => 'width: 30%; text-align:left'],
            ],
            [
                'attribute' => 'primer_vencimiento',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'primer_vencimiento',
                    'language' => 'es',
                    'pickerButton' => false,
                    'options' => [
                        'style' => 'width:90px',
                    ],
                    'pluginOptions' => [
                        'orientation' => 'bottom center',
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'weekStart' => 0,
                    ]
                ]),
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->primer_vencimiento == '') return '';
                    return date('d-m-Y', strtotime($model->primer_vencimiento));
                },
                'contentOptions' => ['style' => 'width: 13%; text-align:center'],
            ],
            [
                'attribute' => 'fecha_vencimiento',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fecha_vencimiento',
                    'language' => 'es',
                    'pickerButton' => false,
                    'options' => [
                        'style' => 'width:90px',
                    ],
                    'pluginOptions' => [
                        'orientation' => 'bottom center',
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'weekStart' => 0,
                    ]
                ]),
                'format' => 'html',
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->fecha_vencimiento));
                },
                'contentOptions' => ['style' => 'width: 13%; text-align:center'],
            ],
            [
                'attribute' => 'no_vencido',
                'label' => "Por Vencer?",
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model EmpresaCalendario */
                    $fa_icon = ($model->fecha_vencimiento < date('Y-m-d') ? 'fa fa-times text-danger' : 'fa fa-check text-success');
                    return "<span class='$fa_icon'></span>";
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'no_vencido',
                    'data' => ['si' => 'Si', 'no' => 'No'],
                    'hideSearch' => false,
                    'pluginOptions' => [
                        'width' => '120px',
                        'allowClear' => true,
                        'placeholder' => 'Todos',
                    ]
                ]),
                'contentOptions' => ['style' => 'width: 5%; text-align:center'],
            ],
            [
                'attribute' => 'frecuencia',
                'value' => function ($model, $key, $index, $column) {
                    return ucfirst($model->frecuencia);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'frecuencia',
                    'data' => array_map('ucfirst', EmpresaCalendario::getValoresEnum('frecuencia')),
                    'hideSearch' => false,
                    'pluginOptions' => [
                        'width' => '120px',
                        'allowClear' => true,
                        'placeholder' => 'Todos',
                    ]
                ]),
                'contentOptions' => ['style' => 'width: 8%; text-align:left'],
            ],
            [
                'attribute' => 'detalle',
                'contentOptions' => ['style' => 'width: 30%; text-align:left'],
            ],
            [
                'attribute' => 'estado',
                'label' => 'Estado',
                'value' => function ($model, $key, $index, $column) {
                    return $model->estado == 'inactivo' ? 'Inactivo' : 'Activo';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'estado',
                    'data' => ['activo' => 'Activo', 'inactivo' => 'Inactivo'],
                    'hideSearch' => false,
                    'pluginOptions' => [
                        'width' => '120px',
                        'allowClear' => true,
                        'placeholder' => 'Todos',
                    ]
                ])
            ],
            [
                'attribute' => 'usuario_id',
                'label' => "Usuario asignado",
                'value' => function ($model) {
                    if ($model->usuario_id == '')
                        return '';

                    return User::findOne($model->usuario_id)->username;
                },
                'filter' => Select2::widget(array(
                    'model' => $searchModel,
                    'attribute' => 'usuario_id',
                    'hideSearch' => false,
                    'data' => ArrayHelper::map(
                        User::find()
                            ->select(['id' => 'core_usuario.id', 'text' => 'username'])
                            ->innerJoin('cal_empresa_calendario calendario', 'calendario.usuario_id = core_usuario.id')
                            ->asArray()->all(), 'id', 'text'
                    ),
                    'pluginOptions' => array(
                        'width' => '120px',
                        'allowClear' => true,
                        'placeholder' => 'Todos',
                    )
                ))
            ],
            ['class' => 'yii\grid\ActionColumn', 'buttons' => [], 'template' => $template,]
        ],
    ]);
    Pjax::end();

} catch (Exception $e) {
    throw $e;
}
Pjax::end();
?>

<?php ob_start(); // output buffer the javascript to register later ?>
<script>
    // var row_expanded = '';
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean()), View::POS_HEAD); ?>

<?php ob_start(); // output buffer the javascript to register later ?>
<script>
    // var $grid = $('#calendario-grid');
    // $grid.on('kvexprow:toggle', function (event, ind, key, extra, state) {
    //     row_expanded = key;
    // });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
