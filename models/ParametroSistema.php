<?php

namespace backend\modules\calendario\models;

use backend\models\BaseModel;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "cont_parametro_sistema".
 *
 * @property string $id
 * @property string $nombre
 * @property string $valor
 */
class ParametroSistema extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cal_parametro_sistema';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'valor'], 'required'],
            [['nombre', 'valor'], 'string', 'max' => 2000],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => Yii::t('app', 'Nombre'),
            'valor' => Yii::t('app', 'Valor'),
        ];
    }

    public static function getValorByNombre($nombre)
    {
        /** @var ParametroSistema $model */
        $model = ParametroSistema::find()->where(['nombre' => $nombre])->one();
        return $model ? $model->valor : '';
    }
}
