<?php

namespace backend\modules\calendario\models\search;

use backend\modules\calendario\models\EmpresaCalendario;
use common\helpers\PermisosHelpers;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EmpresaCalendarioSearch represents the model behind the search form of `backend\modules\calendario\models\EmpresaCalendario`.
 */
class EmpresaCalendarioSearch extends EmpresaCalendario
{
    public $nombre_empresa;
    public $no_vencido;
    public $parent_id = null;
    public $onlyWithFrecuency = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'empresa_id'], 'integer'],
            [['fecha_vencimiento', 'primer_vencimiento', 'frecuencia', 'detalle', 'estado', 'nombre_empresa',
                'no_vencido', 'usuario_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpresaCalendario::find()
            ->joinWith('empresa AS empresa')
            ->leftJoin("core_usuario usuario", 'usuario.id = cal_empresa_calendario.usuario_id');
        $query->select(['cal_empresa_calendario.*', 'empresa.nombre']);
        $query->indexBy('id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $today = date('Y-m-d');
        $dataProvider->sort->attributes['nombre_empresa'] = ['asc' => ['empresa.nombre' => SORT_ASC], 'desc' => ['empresa.nombre' => SORT_DESC], 'label' => 'Empresa', 'default' => SORT_ASC];
        $dataProvider->sort->attributes['no_vencido'] = [
            'asc' => ["DATEDIFF(cont_empresa_calendario.fecha_vencimiento, '{$today}')" => SORT_ASC],
            'desc' => ["DATEDIFF(cont_empresa_calendario.fecha_vencimiento, '{$today}')" => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cal_empresa_calendario.id' => $this->id,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'frecuencia' => $this->frecuencia,
            'primer_vencimiento' => implode('-', array_reverse(explode('-', trim($this->primer_vencimiento)))),
            'fecha_vencimiento' => implode('-', array_reverse(explode('-', trim($this->fecha_vencimiento)))),
        ]);

        $query->andFilterWhere(['like', 'empresa.nombre', $this->nombre_empresa])
            ->andFilterWhere(['like', 'detalle', $this->detalle])
            ->andFilterWhere(['estado' => $this->estado == '0' ? null : $this->estado])
            ->andFilterWhere(['OR', ['like', 'usuario.username', $this->usuario_id], ['=', 'cal_empresa_calendario.usuario_id', $this->usuario_id]]);

        if ($this->no_vencido == 'si') {
            $query->andWhere(['>=', 'fecha_vencimiento', date('Y-m-d')]);
        } elseif ($this->no_vencido == 'no') {
            $query->andWhere(['<', 'fecha_vencimiento', date('Y-m-d')]);
        }

        if (isset($this->parent_id))
            $query->andWhere(['calendario_id' => $this->parent_id]);
        if ($this->onlyWithFrecuency)
            $query->andWhere([
                'OR',
                [
                    'AND',
                    ['=', 'frecuencia', 'unico'],
                    ['IS', 'calendario_id', null]
                ],
                ['!=', 'frecuencia', 'unico']
            ]);

        if (!PermisosHelpers::esSuperUsuario())
            $query->andFilterWhere(['usuario_id' => \Yii::$app->user->id]);

        return $dataProvider;
    }
}
