<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/* @var $model backend\modules\calendario\models\EmpresaCalendario */

$this->title = Yii::t('app', 'Nuevo Calendario de Actividades');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Calendarios de Actividades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
echo Html::beginTag('div', ['class' => 'empresa-calendario-create']);

echo $this->render('_form', [
    'model' => $model,
]);

echo Html::endTag('div');
?>

