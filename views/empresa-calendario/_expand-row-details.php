<?php

use backend\modules\calendario\models\EmpresaCalendario;
use backend\modules\calendario\models\search\EmpresaCalendarioSearch;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;

/* @var $model EmpresaCalendario */
/* @var $subSearchModel backend\modules\calendario\models\search\EmpresaCalendarioSearch */
/* @var $subDataProvider yii\data\ActiveDataProvider */
?>

<?php
$subSearchModel = new EmpresaCalendarioSearch();
$subSearchModel->parent_id = $model->id;
$subDataProvider = $subSearchModel->search(Yii::$app->request->queryParams);
try {
    $template = '';
    foreach (['view', 'update', 'delete'] as $_v) if (PermisosHelpers::getAcceso("empresa-calendario-{$_v}")) $template .= "&nbsp{{$_v}}";
    echo GridView::widget([
        'dataProvider' => $subDataProvider,
//        'filterModel' => $subSearchModel,
        'pjax' => true,
        'export' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            ['attribute' => 'nombre_empresa',
                'value' => 'empresa.nombre',
                'label' => 'Empresa',
                'contentOptions' => ['style' => 'width: 30%; text-align:left'],
            ],
            [
                'attribute' => 'fecha_vencimiento',
                'format' => 'html',
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->fecha_vencimiento));
                },
                'contentOptions' => ['style' => 'width: 8%; text-align:center'],
            ],
            [
                'attribute' => 'no_vencido',
                'label' => "Por Vencer?",
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model EmpresaCalendario */
                    $fa_icon = ($model->fecha_vencimiento < date('Y-m-d') ? 'fa fa-times text-danger' : 'fa fa-check text-success');
                    return "<span class='$fa_icon'></span>";
                },
                'contentOptions' => ['style' => 'width: 5%; text-align:center'],
            ],
            [
                'attribute' => 'frecuencia', 'value' => function ($model, $key, $index, $column) {
                return ucfirst($model->frecuencia);
            },
                'contentOptions' => ['style' => 'width: 8%; text-align:left'],
            ],
            [
                'attribute' => 'detalle',
                'contentOptions' => ['style' => 'width: 30%; text-align:left'],
            ],

//            [
//                'class' => 'kartik\grid\EditableColumn',
//                'header' => 'Estado (editable)',
//                'attribute' => 'estado',
//                'value' => function ($model) {
//                    return $model->estado;
//                },
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'format' => Editable::FORMAT_LINK,
//                        'asPopover' => false,
//                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
//                        'inlineSettings' => [
//                            'options' => ['class' => 'panel panel-default min-panel'],
//                        ],
//                        'data' => ['activo' => "Activo", 'inactivo' => "Inactivo"],
//                        'options' => ['class' => 'form-control', 'prompt' => 'Seleccione un estado...'],
//                        'displayValueConfig' => [
//                            'activo' => 'Activo',
//                            'inactivo' => 'Inactivo',
//                        ],
//                        'editableButtonOptions' => [
//                            'label' => 'editar',
//                        ],
//                        'pluginEvents' => [
//                            "editableSuccess" => "function(event, val, form, data) { afterAjaxUpdate(); }",
//                        ],
//                    ];
//                }
//            ],
            [
                'attribute' => 'estado',
                'label' => 'Estado',
                'value' => function ($model, $key, $index, $column) {
                    return $model->estado == 'inactivo' ? 'Inactivo' : 'Activo';
                },
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons' => [], 'template' => $template,]
        ],
    ]);

} catch (Exception $exception) {
    echo $exception;
}

ob_start(); // output buffer the javascript to register later ?>
    <script>
        function afterAjaxUpdate() {
            // $.pjax.reload({container: "#calendario-grid", async: false});
            // let element = $("tr[data-key='" + row_expanded + "']").find('span.glyphicon-expand');
            // if (element.length > 0) {
            //     element.click();
            //     console.log(element);
            // } else {
            //     console.log("Element no existe: ", element);
            // }
        }
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>