<?php


namespace backend\modules\calendario\helpers;


use backend\modules\calendario\models\EmpresaCalendario;
use common\helpers\PermisosHelpers;
use common\models\User;
use Exception;
use kartik\grid\GridView;
use Yii;
use yii\data\ArrayDataProvider;
use yiister\adminlte\widgets\Box;

class CalendarioViewHelper
{
    public static function NotificationsForCalendario()
    {
        $empresa_id = Yii::$app->session->get('core_empresa_actual', null);

        if (!isset($empresa_id))
            return null;

        $toExpire = 7;
        $today = date('Y-m-d');
        $allModels = EmpresaCalendario::find()
            ->where([
                'estado' => 'activo', 'empresa_id' => $empresa_id, 'frecuencia' => 'unico'
            ])
            ->andWhere(['<=', "DATEDIFF(fecha_vencimiento, '{$today}')", $toExpire]);

        if (!PermisosHelpers::esSuperUsuario())
            $allModels->andFilterWhere(['usuario_id' => \Yii::$app->user->id]);

        $allModels = $allModels->all();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (sizeof($dataProvider->allModels) == 0)
            return null;

        ob_start();
        Box::begin([
            "header" => "Actividades a vencer en $toExpire dias",
            "type" => Box::TYPE_PRIMARY,
            "filled" => true,
        ]);
        try {
            $template = '';
            foreach (['view'] as $_v) if (PermisosHelpers::getAcceso("calendario-empresa-calendario-{$_v}")) $template .= "&nbsp{{$_v}}";

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'rowOptions' => function ($model) {
                    /** @var $model EmpresaCalendario */

                    $fechaAct = date('Y-m-d');
                    $fechaVenc = trim($model->fecha_vencimiento);

                    if ($fechaVenc < $fechaAct)
                        return ['class' => 'danger'];
                    elseif ($fechaVenc == $fechaAct)
                        return ['class' => 'warning'];

                    return ['class' => 'primary'];
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'fecha_vencimiento',
                        'format' => ['date', 'php:d-m-Y']
                    ],
                    'detalle',
                    [
                        'label' => "Estado",
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var $model EmpresaCalendario */

                            $fechaAct = strtotime(date('Y-m-d'));
                            $fechaVenc = strtotime(trim($model->fecha_vencimiento));
                            $diff = round(abs($fechaVenc - $fechaAct) / (60 * 60 * 24));

                            if ($fechaVenc < $fechaAct)
                                return "VENCIDO hace " . $diff . " día(s)";

                            if ($fechaAct == $fechaVenc)
                                return 'VENCE HOY';

                            return "Faltan {$diff} día(s)";
                        }
                    ],
                    [
                        'label' => "Usuario asignado",
                        'value' => function ($model) {
                            if ($model->usuario_id == '')
                                return '';

                            return User::findOne($model->usuario_id)->username;
                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['class' => 'text-left'],
                        'header' => 'Acciones',
                        'buttons' => [],
                        'template' => $template,
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'view') {
                                $url = 'index.php?r=calendario/empresa-calendario/view&id=' . $model->id;
                                return $url;
                            }
                            return '';
                        }
                    ]
                ],
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        Box::end();

        $notificationBox = ob_get_clean();
        return $notificationBox;
    }
}