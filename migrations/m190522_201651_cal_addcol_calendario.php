<?php

use jamband\schemadump\Migration;

class m190522_201651_cal_addcol_calendario extends Migration
{
    public function safeUp()
    {
        $table = 'cal_empresa_calendario';
        $this->addColumn($table, 'calendario_id', $this->integer(10)->unsigned()->null()->comment("id de la actividad padre."));
        $this->addForeignKey('fk_calendario_id', $table, 'calendario_id', $table, 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
