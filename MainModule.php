<?php

namespace backend\modules\calendario;

use common\helpers\PermisosHelpers;
use common\interfaces\ModuleInterface;
use yii\base\Module;


/**
 * administracion module definition class
 */
class MainModule extends Module implements ModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\calendario\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * Retorna los ítems de menú del módulo.
     * @return mixed
     */
    public static function getMenuItems()
    {
        $items = array();

        //Agregar Menú Items aquí
        $subitems_empresa_calendario = [];

        if (PermisosHelpers::getAcceso('calendario-empresa-calendario-index')) {
            array_push($items, ['label' => 'Calendario de actividades', 'url' => ['/calendario/empresa-calendario/index'], 'icon' => 'far fa-calendar', 'active' => \Yii::$app->controller->id == 'empresa-calendario']);
        }
        return $items;
    }

    /**
     * Retorna la lista de variables de sesión del modulo, dependientes de la empresa actual seleccionada.
     * @return mixed
     */
    public static function getSessionVariablesDepEmpresa()
    {
        $items = array();

        return $items;
    }

    public static function getSessionVariables()
    {
        return [
            'cal_empresa_calendario',
        ];
    }
}
