<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\calendario\models\EmpresaCalendario */

$this->title = Yii::t('app', 'Modificar Calendario de Actividades');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Calendarios de Actividades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-calendario-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
