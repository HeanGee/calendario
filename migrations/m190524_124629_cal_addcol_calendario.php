<?php

use jamband\schemadump\Migration;

class m190524_124629_cal_addcol_calendario extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cal_empresa_calendario', 'primer_vencimiento', $this->date()->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
