<?php

use backend\modules\calendario\models\EmpresaCalendario;
use common\helpers\PermisosHelpers;
use common\models\User;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\calendario\models\EmpresaCalendario */
/* @var $form ActiveForm */
?>

<?php
$action_id = Yii::$app->controller->action->id;
?>

<?php $form = ActiveForm::begin(['id' => 'empresa-calendario-form']); ?>

<?php
try {
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => false,
        'rows' => [
            [
                'autoGenerateColumns' => false, 'columns' => 6,
                'attributes' => [
                    'primer_vencimiento' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '1'],
                        'placeholder' => 'Fecha',
                        'value' => $form->field($model, 'primer_vencimiento')->widget(DateControl::className(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'php:d-m-Y',
                            'saveFormat' => 'php:Y-m-d',
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'disabled' => !empty($model->getChildsActivity()),
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'weekStart' => '0',
                                    'autoclose' => true,
                                    'todayHighlight' => true,
                                ],
                            ],

                        ])->label('1er. Vencimiento'),
                    ],
                    'fecha_vencimiento' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '1'],
                        'placeholder' => 'Fecha',
                        'value' => $form->field($model, 'fecha_vencimiento')->widget(DateControl::className(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'php:d-m-Y',
                            'saveFormat' => 'php:Y-m-d',
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'disabled' => !empty($model->getChildsActivity()),
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'weekStart' => '0',
                                    'autoclose' => true,
                                    'todayHighlight' => true,
                                ],
                            ],

                        ])->label('Vencimiento'),
                    ],
                    'detalle' => [
                        'type' => Form::INPUT_TEXT,
                        'columnOptions' => ['colspan' => '2'],
                    ],
                ],
            ],
            [
                'autoGenerateColumns' => false, 'columns' => 6,
                'attributes' => [
                    'frecuencia' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'frecuencia')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione...'],
                            'data' => ['unico' => 'Unico',
                                'mensual' => 'Mensual',
                                'bimensual' => 'Bimensual',
                                'trimestral' => 'Trimestral',
                                'cuatrimestral' => 'Cuatrimestral',
                                'semestral' => 'Semestral',
                                'anual' => 'Anual',
                            ],
                            'disabled' => !PermisosHelpers::esSuperUsuario() && isset($model->calendarioPadre),
                        ])
                    ],
                    'estado' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => Select2::className(), 'options' => ['data' => EmpresaCalendario::getEstados(false),],],
                    'usuario_id' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'usuario_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione...'],
                            'data' => ArrayHelper::map(User::find()->select(['id', "username as text"])->asArray()->all(), 'id', 'text'),
                            'disabled' => !PermisosHelpers::esSuperUsuario(),
                        ])->label("Usuario asignado")
                    ],
                ],
            ],
            [
                'attributes' => [
                    'action' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="margin-top: 20px">' .
                            Html::submitButton('Guardar', ['class' => 'btn btn-primary']) .
                            '</div>'
                    ],
                ],
            ]
        ]
    ]);
} catch (Exception $e) {
    print $e;
}
?>

<?php ActiveForm::end(); ?>

<?php ob_start(); // output buffer the javascript to register later ?>
    <script>
        $("#empresacalendario-frecuencia").change(function (evt) {
            let element = $("#empresacalendario-primer_vencimiento-disp");
            let value = element.val();
            if (value === 'unico') {
                $('form[id^="empresa-calendario-form"]').yiiActiveForm('validateAttribute', 'empresacalendario-primer_vencimiento');
            } else {
                $('form[id^="empresa-calendario-form"]').yiiActiveForm('validateAttribute', 'empresacalendario-primer_vencimiento');
            }
        });

        function setPopOver(element = null, title = "", msg = "", type = "info-l") {
            if (element === null)
                return;

            applyPopOver(element, title, msg, {'theme-color': type, 'placement': 'top'});
        }

        $(document).on('change', '#empresacalendario-frecuencia, #empresacalendario-fecha_vencimiento-disp, #empresacalendario-primer_vencimiento-disp', function () {
            $('form[id="empresa-calendario-form"]').yiiActiveForm('validateAttribute', 'empresacalendario-frecuencia');
        });

        $(document).ready(function () {
            if ('<?= $action_id ?>' === 'update' && $('#empresacalendario-frecuencia').val() !== 'unico') {
                let label = $('div.field-empresacalendario-fecha_vencimiento');
                setPopOver(label, 'Por qué está bloqueado?', "Esta actividad tiene sub-actividades, por tanto no puede se modificar.", 'warning-l');
                label = $('div.field-empresacalendario-primer_vencimiento');
                setPopOver(label, 'Por qué está bloqueado?', "Esta actividad tiene sub-actividades, por tanto no puede se modificar.", 'warning-l');

                let permiso = "<?= PermisosHelpers::esSuperUsuario() ?>";
                if (permiso === "") {
                    let element = $('#empresacalendario-usuario_id');
                    element.val('').trigger('change');
                    element.parent().parent().hide();
                }
            }
        });
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>