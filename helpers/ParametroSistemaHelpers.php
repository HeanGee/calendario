<?php

namespace backend\modules\calendario\helpers;

use backend\modules\calendario\models\ParametroSistema;

/**
 * Class ParametroSistemaHelpers
 * @package common
 *
 */
class ParametroSistemaHelpers
{
    public static function getValorByNombre($nombre)
    {
        /** @var ParametroSistema $model */
        $model = ParametroSistema::find()->where(['nombre' => $nombre])->one();
        return $model ? $model->valor : '';
    }

    public static function getNombreByValor($valor)
    {
        /** @var ParametroSistema $model */
        $model = ParametroSistema::find()->where(['valor' => $valor])->one();
        return $model ? $model->nombre : '';
    }
}
