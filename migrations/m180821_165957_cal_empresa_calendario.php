<?php

use jamband\schemadump\Migration;

class m180821_165957_cal_empresa_calendario extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cal_empresa_calendario}}', [
            'id' => $this->primaryKey()->unsigned(),
            'empresa_id' => $this->integer(10)->unsigned()->null(),
            'fecha_vencimiento' => $this->date()->notNull(),
            'frecuencia' => "ENUM ('unico', 'mensual', 'bimensual', 'trimestral', 'cuatrimestral', 'semestral', 'anual') NOT NULL",
            'detalle' => $this->string(55)->notNull(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL",
        ], $this->tableOptions);

        //fk: con core_calendario
        $this->addForeignKey('fk_cal_empresa_calendario_empresa_id', '{{%cal_empresa_calendario}}', 'empresa_id', '{{%core_empresa}}', 'id');

    }

    public function safeDown()
    {
        echo "m180821_165957_cal_empresa_calendario no puede ser revertido.\n";
        return false;
    }
}
