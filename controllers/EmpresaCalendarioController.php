<?php

namespace backend\modules\calendario\controllers;

use backend\controllers\BaseController;
use backend\modules\calendario\models\EmpresaCalendario;
use backend\modules\calendario\models\search\EmpresaCalendarioSearch;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use Throwable;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * EmpresaCalendarioController implements the CRUD actions for EmpresaCalendario model.
 */
class EmpresaCalendarioController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmpresaCalendario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpresaCalendarioSearch();
        $searchModel->onlyWithFrecuency = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');
            $model = EmpresaCalendario::findOne($id);
            $output = Json::encode(['output' => '', 'message' => '']);

            try {
                $post['EmpresaCalendario'] = array_values(Yii::$app->request->post("EmpresaCalendario"))[0];
                if ($model->load($post)) {
                    if (!$model->save()) {
                        throw new \Exception("No se pudo guardar: {$model->getErrorSummaryAsString()}");
                    }
                } else {
                    throw new \Exception("No se pudo loadear");
                }
            } catch (\Exception $exception) {
                $output = Json::encode(['output' => 'error', 'message' => $exception->getMessage()]);
            }
            echo $output;
            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmpresaCalendario model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new EmpresaCalendarioSearch();
        $searchModel->parent_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');
            $model = EmpresaCalendario::findOne($id);
            $output = Json::encode(['output' => '', 'message' => '']);

            try {
                $post['EmpresaCalendario'] = array_values(Yii::$app->request->post("EmpresaCalendario"))[0];
                if ($model->load($post)) {
                    if (!$model->save()) {
                        throw new \Exception("No se pudo guardar: {$model->getErrorSummaryAsString()}");
                    }
                } else {
                    throw new \Exception("No se pudo loadear");
                }
            } catch (\Exception $exception) {
                $output = Json::encode(['output' => 'error', 'message' => $exception->getMessage()]);
            }
            echo $output;
            return;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new EmpresaCalendario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmpresaCalendario();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->empresa_id = $_SESSION['core_empresa_actual'];
                if (!PermisosHelpers::esSuperUsuario()) {
                    $model->usuario_id = "";
                }

                if (!$model->save())
                    throw new \Exception("No se pudo guardar: {$model->getErrorSummaryAsString()}");

                if ($model->frecuencia != 'unico') {
                    $model->refresh();
                    $expireAt = $model->primer_vencimiento;
                    $expireDates = [$expireAt];
                    while (true) {
                        switch ($model->frecuencia) {
                            case 'mensual':
                                $expireAt = date('Y-m-d', strtotime("+1 month", strtotime($expireAt)));
                                break;
                            case 'bimensual':
                                $expireAt = date('Y-m-d', strtotime("+2 months", strtotime($expireAt)));
                                break;
                            case 'trimestral':
                                $expireAt = date('Y-m-d', strtotime("+3 months", strtotime($expireAt)));
                                break;
                            case 'cuatrimestral':
                                $expireAt = date('Y-m-d', strtotime("+4 months", strtotime($expireAt)));
                                break;
                            case 'semestral':
                                $expireAt = date('Y-m-d', strtotime("+6 months", strtotime($expireAt)));
                                break;
                            case 'anual':
                                $expireAt = date('Y-m-d', strtotime("+1 year", strtotime($expireAt)));
                                break;
                        }

                        if ($expireAt <= $model->fecha_vencimiento)
                            $expireDates[] = $expireAt;
                        else
                            break;
                    }
                    foreach ($expireDates as $expireDate) {
                        if (EmpresaCalendario::find()->where(['calendario_id' => $model->id, 'fecha_vencimiento' => $expireDate])->exists()) {
                            continue;
                        }
                        $newActivity = new EmpresaCalendario();
                        $newActivity->estado = 'activo';
                        $newActivity->frecuencia = 'unico';
                        $newActivity->calendario_id = $model->id;
                        $newActivity->detalle = $model->detalle;
                        $newActivity->empresa_id = $model->empresa_id;
                        $newActivity->usuario_id = $model->usuario_id;
                        $newActivity->fecha_vencimiento = $expireDate;
                        if (!$newActivity->save()) {
                            throw new \Exception("Error al generar actividades únicas: {$newActivity->getErrorSummaryAsString()}");
                        }
                    }
                }
                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('El calendario de actividades se ha creado exitosamente');
                return $this->redirect(['index']);
            } catch (\Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EmpresaCalendario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $usuario_asignado_id = $model->usuario_id;
        $frecuencia = $model->frecuencia;

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!PermisosHelpers::esSuperUsuario()) {
                    $model->usuario_id = $usuario_asignado_id;
                    $model->frecuencia = $frecuencia;
                }

                if (!$model->save()) {
                    throw new Exception('No se pudo validar el formulario');
                }

                if ($model->frecuencia != 'unico') {
                    $model->refresh();
                    $expireAt = $model->primer_vencimiento;
                    $expireDates = [$expireAt];
                    while (true) {
                        switch ($model->frecuencia) {
                            case 'mensual':
                                $expireAt = date('Y-m-d', strtotime("+1 month", strtotime($expireAt)));
                                break;
                            case 'bimensual':
                                $expireAt = date('Y-m-d', strtotime("+2 months", strtotime($expireAt)));
                                break;
                            case 'trimestral':
                                $expireAt = date('Y-m-d', strtotime("+3 months", strtotime($expireAt)));
                                break;
                            case 'cuatrimestral':
                                $expireAt = date('Y-m-d', strtotime("+4 months", strtotime($expireAt)));
                                break;
                            case 'semestral':
                                $expireAt = date('Y-m-d', strtotime("+6 months", strtotime($expireAt)));
                                break;
                            case 'anual':
                                $expireAt = date('Y-m-d', strtotime("+1 year", strtotime($expireAt)));
                                break;
                        }

                        if ($expireAt <= $model->fecha_vencimiento)
                            $expireDates[] = $expireAt;
                        else
                            break;
                    }
                    foreach ($expireDates as $expireDate) {
                        if (EmpresaCalendario::find()->where(['calendario_id' => $model->id, 'fecha_vencimiento' => $expireDate])->exists()) {
                            continue;
                        }
                        $newActivity = new EmpresaCalendario();
                        $newActivity->estado = 'activo';
                        $newActivity->frecuencia = 'unico';
                        $newActivity->calendario_id = $model->id;
                        $newActivity->detalle = $model->detalle;
                        $newActivity->empresa_id = $model->empresa_id;
                        $newActivity->usuario_id = $model->usuario_id;
                        $newActivity->fecha_vencimiento = $expireDate;
                        if (!$newActivity->save()) {
                            throw new \Exception("Error al generar actividades únicas: {$newActivity->getErrorSummaryAsString()}");
                        }
                    }
                }
                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('El calendario de actividades se ha modificado exitosamente');
                return $this->redirect(['index']);
            } catch (\Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmpresaCalendario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!empty($model->getChildsActivity())) {
//                throw new \Exception("Esta actividad tiene sub-actividades. Por lo tanto no se puede borrar.");
                foreach ($model->getChildsActivity() as $child) {
                    if (!$child->delete())
                        throw new \Exception("Error borrando sub-tarea (id #{$child->id}: {$child->getErrorSummaryAsString()}");
                    $child->refresh();
                }
            }

            if (!$model->delete())
                throw new \Exception("Error al borrar actividad: {$model->getErrorSummaryAsString()}");;

            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage("Esta actividad fue eliminada exitosamente.");
        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmpresaCalendario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EmpresaCalendario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmpresaCalendario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}
