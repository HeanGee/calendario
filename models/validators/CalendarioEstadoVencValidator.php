<?php

namespace backend\modules\calendario\models\validators;

use backend\modules\calendario\models\EmpresaCalendario;
use yii\validators\Validator;

class CalendarioEstadoVencValidator extends Validator
{
    const ERROR_MSG = "Si la frecuencia es UNICO, el 1er. Vencimiento y Vencimiento deben ser iguales o, solamente especificar Vencimiento.";

    public function validateAttribute($model, $attribute)
    {
        /** @var $model EmpresaCalendario */
        if ($model->frecuencia == 'unico') {
            if ($model->primer_vencimiento != '' && $model->primer_vencimiento != $model->fecha_vencimiento)
                $this->addError($model, $attribute, self::ERROR_MSG);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg = self::ERROR_MSG;
        return <<<JS
if($("#empresacalendario-frecuencia").val() === 'unico') {
    let primerVenc = $("#empresacalendario-primer_vencimiento");
    if (primerVenc.val() !== '' && primerVenc.val() !== $("#empresacalendario-fecha_vencimiento").val()) {
        messages.push("{$msg}");
    }
}
JS;
    }
}
