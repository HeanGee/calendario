<?php

namespace backend\modules\calendario\models\validators;

use backend\modules\calendario\models\EmpresaCalendario;
use yii\validators\Validator;

class CalendarioPrimerVencimientoValidator extends Validator
{
    const ERROR_MSG = "Primer Vencimiento es requerido si la frecuencia es diferente a `unica`.";

    public function validateAttribute($model, $attribute)
    {
        /** @var $model EmpresaCalendario */
        if ($model->frecuencia != 'unico') {
            if ($model->primer_vencimiento == '')
                $this->addError($model, $attribute, self::ERROR_MSG);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg = self::ERROR_MSG;
        return <<<JS
if($("#empresacalendario-frecuencia").val() !== 'unico') {
    if ($("#empresacalendario-primer_vencimiento").val() === '') {
        messages.push("{$msg}");
    }
}
JS;
    }
}
