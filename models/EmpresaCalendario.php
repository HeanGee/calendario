<?php

namespace backend\modules\calendario\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\modules\calendario\models\validators\CalendarioEstadoVencValidator;
use backend\modules\calendario\models\validators\CalendarioPrimerVencimientoValidator;
use common\models\User;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cal_empresa_calendario".
 *
 * @property string $id
 * @property string $empresa_id
 * @property string $primer_vencimiento
 * @property string $fecha_vencimiento
 * @property string $frecuencia
 * @property string $detalle
 * @property string $estado
 * @property string $calendario_id
 * @property string $usuario_id
 *
 * @property Empresa $empresa
 * @property array $frecuencyList
 * @property array $frecuency_list
 * @property EmpresaCalendario $calendarioPadre
 * @property User $usuarioAsignado
 */
class EmpresaCalendario extends BaseModel
{
    private $frecuency_list = ['unico', 'mensual', 'bimensual', 'trimestral', 'cuatrimestral', 'semestral', 'anual'];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cal_empresa_calendario';
    }

    public static function getEstados($isForFilteringBy = false)
    {
        $array = [];
        !$isForFilteringBy || $array['0'] = "Todos";
        $array['activo'] = "Activo";
        $array['inactivo'] = "Inactivo";

        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id'], 'integer'],
            [['fecha_vencimiento', 'frecuencia', 'detalle', 'estado'], 'required'],
            [['fecha_vencimiento', 'frecuency_list', 'calendario_id', 'primer_vencimiento', 'usuario_id'], 'safe'],
            [['frecuencia', 'estado', 'primer_vencimiento'], 'string'],
            [['detalle'], 'string', 'max' => 55],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_id' => 'id']],

            [['primer_vencimiento'], CalendarioPrimerVencimientoValidator::className()],
            [['frecuencia'], CalendarioEstadoVencValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

        return EmpresaCalendario::getAttributesLabelText();
    }

    public function getFrecuencyList()
    {
        return $this->frecuency_list;
    }

    public static function getAttributesLabelText()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa',
            'fecha_vencimiento' => 'Fecha de Vencimiento',
            'frecuencia' => 'Frecuencia',
            'detalle' => 'Detalles',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioAsignado()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalendarioPadre()
    {
        return $this->hasOne(self::className(), ['id' => 'calendario_id']);
    }

    /**
     * @return EmpresaCalendario[]
     */
    public function getChildsActivity()
    {
        if (!isset($this->id) || $this->id == '')
            return [];

        return self::findAll(['calendario_id' => $this->id]);
    }

    /**
     * @return EmpresaCalendario[]
     */
    public function getActiveChildsActivity()
    {
        return self::findAll(['calendario_id' => $this->id, 'estado' => 'activo', 'frecuencia' => 'unica']);
    }

    /**
     * @return EmpresaCalendario[]
     */
    public function getInactiveChildsActivity()
    {
        return self::findAll(['calendario_id' => $this->id, 'estado' => 'inactivo', 'frecuencia' => 'unica']);
    }

    public static function getEmpresaLista()
    {
        $dropciones = Empresa::find()->asArray()->all();
        return ArrayHelper::map($dropciones, 'id', 'razon_social');
    }

    public static function getValoresEnum($columna, $paraForm = false)
    {
        $_retornar = [];
        try {
            $valores_enum = EmpresaCalendario::getTableSchema()->columns[$columna]->enumValues;
            if ($paraForm) {
                foreach ($valores_enum as $_v) $_retornar[] = ['id' => $_v, 'text' => ucfirst($_v)];
            } else {
                foreach ($valores_enum as $_v) $_retornar[$_v] = ucfirst($_v);
            }

        } catch (Exception $e) {
            /* TODO: manejar la excepción */
        }
        return $_retornar;
    }

    public function beforeValidate()
    {
        $this->primer_vencimiento = ($this->frecuencia == 'unico') ? '' : trim($this->primer_vencimiento);
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * @throws Exception
     */
    public static function generateActivitiesForNotifications()
    {
        $fn_name = __FUNCTION__;
        $early = false;

        $today = date("Y-m-d");

        /** @var EmpresaCalendario[] $activityTemplates */
        $activityTemplates = self::find()
            ->orderBy([
                'empresa_id' => SORT_ASC,
                'fecha_vencimiento' => SORT_ASC,

            ])
            ->where(['estado' => "activo"])
            ->andWhere(['!=', 'frecuencia', 'unico'])
            ->andWhere(['>', 'fecha_vencimiento', $today])
            ->all();

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($activityTemplates as $model) {
                if ($model->fecha_vencimiento < $today) {
//                    self::stdout("Calendario $model->id se pasa de largo por su fecha de vencimiento $model->fecha_vencimiento.");
                    continue;
                }

//                if (!empty($model->getChildsActivity())) {
//                    self::stdout("Calendario $model->id se pasa de largo porque tiene hijos.");
//                    continue;
//                }

                $expireAt = $model->fecha_vencimiento;
                $expireDates = [$expireAt];
                while (true) {
                    switch ($model->frecuencia) {
                        case 'mensual':
                            $expireAt = date('Y-m-d', strtotime("-1 month", strtotime($expireAt)));
                            break;
                        case 'bimestral':
                            $expireAt = date('Y-m-d', strtotime("-2 months", strtotime($expireAt)));
                            break;
                        case 'trimestral':
                            $expireAt = date('Y-m-d', strtotime("-3 months", strtotime($expireAt)));
                            break;
                        case 'cuatrimestral':
                            $expireAt = date('Y-m-d', strtotime("-4 months", strtotime($expireAt)));
                            break;
                        case 'semestral':
                            $expireAt = date('Y-m-d', strtotime("-6 months", strtotime($expireAt)));
                            break;
                        case 'anual':
                            $expireAt = date('Y-m-d', strtotime("-1 year", strtotime($expireAt)));
                            break;
                    }

                    if ($expireAt >= $today)
                        $expireDates[] = $expireAt;
                    else
                        break;
                }
                foreach (array_reverse($expireDates) as $expireDate) {
                    if (self::find()->where(['calendario_id' => $model->id, 'fecha_vencimiento' => $expireDate])->exists()) {
//                        self::stdout("Nuevo calendario con $expireDate no se crea porque ya existe.");
                        continue;
                    }
                    $newActivity = new self();
                    $newActivity->estado = 'activo';
                    $newActivity->frecuencia = 'unico';
                    $newActivity->calendario_id = $model->id;
                    $newActivity->detalle = $model->detalle;
                    $newActivity->empresa_id = $model->empresa_id;
                    $newActivity->fecha_vencimiento = $expireDate;
                    if (!$newActivity->save()) {
                        self::stdout("Error guardando newActivity: {$model->getErrorSummaryAsString()}");
                        throw new Exception("Error from `$fn_name()`: Save failed: {$newActivity->getErrorSummaryAsString()}");
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $early ? 1 : 0;
    }

    /**
     * Formato para el log: `<codigo_estado>;<fecha>;<mensaje>PHP_EOL` sin los encerradores y sin espacios al rededor del `;`.
     *
     * @param string $string
     * @param int $deep
     * @param int $statusCode
     */
    private static function stdout($string, $deep = 0, $statusCode = 1)
    {
        $factor = 3;
        $left_margin = str_repeat(' ', $deep * $factor + 1);
        $line = $statusCode . ';' . (date("Y-m-d;G:i:s;l;") . $left_margin . $string . PHP_EOL);
        echo $line;
    }
}
